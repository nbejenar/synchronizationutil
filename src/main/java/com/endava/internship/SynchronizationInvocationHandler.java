package com.endava.internship;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Set;

public class SynchronizationInvocationHandler<T> implements InvocationHandler {

    private final Object lock = new Object();
    private T delegate;

    private static Set<Method> objectMethods = Set.of(Object.class.getDeclaredMethods());

    public SynchronizationInvocationHandler(T target) {
        this.delegate = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (isObjectMethod(method)) {
            System.out.println("Object method");
            return method.invoke(delegate, args);
        }
        synchronized (lock) {
            System.out.println("Synchronized method");
            return method.invoke(delegate, args);
        }
    }

    private boolean isObjectMethod(Method m) {
        return objectMethods.contains(m);
    }
}


