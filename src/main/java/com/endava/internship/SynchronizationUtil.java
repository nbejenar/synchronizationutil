package com.endava.internship;

import net.sf.cglib.proxy.Enhancer;

import java.lang.reflect.Proxy;
import java.util.*;

public final class SynchronizationUtil {

    private SynchronizationUtil() {
    }

    public static <T> T synchronize(T target) {
        Class<?>[] interfaces = getInterfacesRecursively(target);
        if (interfaces.length == 0) {
            return makeCglibSynchronizationProxy(target);
        }
        return makeJdkDynamicSynchronizationProxy(target, interfaces);
    }

    @SuppressWarnings("unchecked")
    private static <T> T makeJdkDynamicSynchronizationProxy(T target, Class<?>[] interfaces) {
        return (T) Proxy.newProxyInstance(
                SynchronizationUtil.class.getClassLoader(),
                interfaces,
                new SynchronizationInvocationHandler(target)
        );
    }

    @SuppressWarnings("unchecked")
    private static <T> T makeCglibSynchronizationProxy(T target) {
        return (T) Enhancer.create(
                target.getClass(),
                new CglibInvocationHandlerAdapter(new SynchronizationInvocationHandler(target))
        );
    }

    private static Class<?>[] getInterfacesRecursively(Object target) {
        Set<Class<?>> interfaces = new HashSet<>();
        Class<?> clazz = target.getClass();
        do {
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
        } while ((clazz = clazz.getSuperclass()) != null);

        return interfaces.toArray(new Class<?>[0]);
    }
}
