package com.endava.internship;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CglibInvocationHandlerAdapter implements net.sf.cglib.proxy.InvocationHandler {
    private final InvocationHandler targetInvocationHandler;

    public CglibInvocationHandlerAdapter(InvocationHandler invocationHandler) {
        targetInvocationHandler = invocationHandler;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return targetInvocationHandler.invoke(proxy, method, args);
    }
}
