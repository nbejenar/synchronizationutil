package com.endava.internship;

import com.endava.internship.util.ClassExtendingParentWithImplements;
import com.endava.internship.util.ClassImplementingInterface;
import com.endava.internship.util.SimpleClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Proxy;

import static org.assertj.core.api.Assertions.assertThat;

class SynchronizationUtilTest {
    @Test
    void noExceptionWhenCreatedProxyOfAClass_withNoImplements() {
        Assertions.assertDoesNotThrow(() -> {
            SimpleClass object = SynchronizationUtil.synchronize(new SimpleClass());
            object.foo();
        });
    }

    @Test
    void noExceptionWhenCreatedProxyOfAClass_withImplements() {
        Assertions.assertDoesNotThrow(() -> {
            AutoCloseable object = SynchronizationUtil.synchronize(new ClassImplementingInterface());
            object.close();
        });
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void shouldCreateJdkProxy() {
        AutoCloseable object = SynchronizationUtil.synchronize(new ClassImplementingInterface());
        assertThat(object instanceof Proxy).isTrue();
    }

    @Test
    void noExceptionWhenCreatedProxyOfAClass_whenParentImplements() {
        Assertions.assertDoesNotThrow(() -> {
            AutoCloseable object = SynchronizationUtil.synchronize(new ClassExtendingParentWithImplements());
            object.close();
        });
    }
}